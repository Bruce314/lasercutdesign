# README #

This repository contains a bunch of hardware schematics for stuff that I'm building.
You can expect to meet 

- svg files, made with inkscape, intended for laser cutting (usually on 3mm thick material)
- scad files, made with openscad, intended for 3D printing

This repository might as well at some point contains some electronic schematics, not the case currently.

Unless specified otherwise, all creative work is CC-BY-SA, there should be node code in this repository.