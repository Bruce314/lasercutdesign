// headphone' headband is roughly 5 cm wide

hp_thick=35;
hp_mat=15;
hp_cable_diam=5.5;

desk_thickness=19;
desk_material=15;
desk_width=30;
desk_length=80;
desk_hanging=hp_mat;
desk_rounding=0;

nut_number=3;
nut_size=10; 
nut_thickness=5;
nut_hole=6; 
nut_limit=nut_hole*2;

nut_offset= (desk_length+desk_width/2-3*nut_limit)/(nut_number-1);

epsilon=0.05;

module nut()
{	
  translate ([0,0,-nut_thickness/2]) union() {
    intersection () {
      cube ([nut_size,nut_size*3,nut_thickness],center = true);
      rotate ([0,0,120]) cube ([nut_size,nut_size*3,nut_thickness],center = true);
      rotate ([0,0,-120]) cube ([nut_size,nut_size*3,nut_thickness],center = true);
    }
    cylinder (h=nut_thickness+desk_material*2+desk_rounding*2,r=nut_hole/2,center=true);
  }
}

module holder_top () {
  rotate([0,90,0])intersection () {
    cylinder (r=desk_width/2,h=hp_mat,center=true);
    rotate ([90,0,0]) cylinder (r=hp_mat/2,h=desk_width,center=true);
  }
}

module headphone_holder()
{
  translate([hp_thick+hp_mat,0,0])
  union () {
    rotate ([0,10,0]) translate ([hp_thick+hp_mat/2,0,0]) holder_top ();
    rotate ([0,90-40,0]) translate ([-hp_thick-hp_mat/2,0,0]) holder_top ();

    rotate ([90,0,0]) difference() {
      cylinder(h=desk_width,r=hp_thick+hp_mat,center=true);
      cylinder(h=desk_width+2*epsilon,r=hp_thick+desk_rounding,center=true);
      rotate ([0,0,-10]) translate([0,0,-desk_width/2-epsilon]) cube([hp_thick+hp_mat,hp_thick+hp_mat,desk_width+2*epsilon]);
      rotate ([0,0,40]) translate([0,0,-desk_width/2-epsilon]) cube([hp_thick+hp_mat,hp_thick+hp_mat,desk_width+2*epsilon]);
    }
  }
}

module desk_side ()
{
  difference() {
    union () 
    {
      union() {
        translate ([-desk_length,-desk_width/2,-desk_material]) cube ([desk_length+desk_hanging,desk_width,desk_material*2+desk_thickness,]);
	translate([0,0,0]) headphone_holder();
	translate ([-desk_length,0,0]) intersection () {
	  translate([0,0,-desk_material]) cylinder(r=desk_width/2,h=desk_material*2+desk_thickness,enter=true);
	  translate ([0,0,desk_thickness/2])rotate ([90,0,0]) cylinder(r=desk_material+desk_thickness/2,h=desk_width,center=true);
	} 
      }
//sphere (r=desk_rounding);
    }
    translate ([-desk_hanging-desk_length,-desk_width,0]) 
      cube ([desk_length+desk_hanging,desk_width*2,desk_thickness,]);
    for (i= [0:nut_number-1]) {
      translate ([-(nut_limit+i*nut_offset),0,epsilon]) nut();
    }
    translate ([0,0,desk_thickness+desk_material-hp_cable_diam/3])rotate([90,0,0]) cylinder (r=hp_cable_diam/2,desk_width+2*epsilon,center=true);
//		translate ([-3*desk_length/4,0,epsilon]) nut() 
  }
}


// nut();
//holder_top();
desk_side();
// headphone_holder();


