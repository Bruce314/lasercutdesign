include <nuts.scad>

epsilon=0.05;
length=18;
thick=06;
height=10;
angle=135;

module side()
{
  difference ()
  {
    union ()
    {
      translate ([0,0,-height/2]) cube ([length,thick,height]);
      translate ([length,0,0]) intersection () 
      {
        translate ([-thick,0,-height/2]) cube ([thick*2,thick,height]);
        cylinder (r=thick,h=height,center=true,$fn=100);
      }
    }
    translate ([3*length/4,thick+epsilon,0]) rotate ([0,30,0]) rotate ([-90,0,0]) nut_m4();
  }
}
module equerre()
{
  side();
  rotate ([0,0,angle-180]) rotate ([0,180,0]) side();

}

equerre ();
