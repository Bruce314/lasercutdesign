
// sizes are in mm
// reference : http://www.fairburyfastener.com/xdims_metric_nuts.htm
// http://www.littlemachineshop.com/reference/tapdrillsizes.pdf

module nut(nut_size,nut_thickness,nut_hole)
{	
  translate ([0,0,-nut_thickness/2]) union () {
    intersection () {
      cube ([nut_size,nut_size*3,nut_thickness],center = true);
      rotate ([0,0,120]) cube ([nut_size,nut_size*3,nut_thickness],center = true);
      rotate ([0,0,-120]) cube ([nut_size,nut_size*3,nut_thickness],center = true);
    }
    cylinder (h=nut_thickness*20,r=nut_hole/2*1.1,center=true,$fn=50);
  }
}


module nut_m2_5(){
  nut(5,2,2.5);
}

module nut_m3(){
  nut(5.5,2.4,3);
}
module nut_m4(){
  nut(7,3.2,4);
}

module nut_m5(){
  nut(8,4.7,5);
}


module nut_m6(){
  nut(10,5.2,6);
}

